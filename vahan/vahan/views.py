from django.http import HttpResponse
import vahan
import json
import pprint


def get_vehicle_info(request, reg_no):
    return HttpResponse(vahan.get_data(reg_no),
                        content_type="application/json")
