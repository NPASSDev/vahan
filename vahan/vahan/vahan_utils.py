from random import randint
from datetime import datetime
import random
import string
def getRandomUserAgent():
	user_agents=[
		"Dalvik/1.6.0 (Linux; U; Android 4.0.4; T970 Build/IMM76D)100.48.122; Profile/MIDP-2.1 Configuration/",
		"Dalvik/1.6.0 (Linux; U; Android 4.2.2; A850 Build/JDQ39) Configuration/CLDC-1.1; Opera Mini/att/4.2.",
		"Dalvik/1.4.0 (Linux; U; Android 2.3.6; GT-S5300 Build/GINGERBREAD)/CLDC-1.1; Opera Mini/att/4.2.",
		"Dalvik/1.4.0 (Linux; U; Android 2.3.6; GT-S5570 Build/GINGERBREAD)",
		"Dalvik/1.4.0 (Linux; U; Android 2.3.6; GT-S5300 Build/GINGERBREAD)",
		"Dalvik/1.4.0 (Linux; U; Android 2.3.6; HUAWEI Y210-0100 Build/HuaweiY210-0100)",
		"Dalvik/1.6.0 (Linux; U; Android 4.2.1; Lenovo A766 Build/JOP40D)",
		"Dalvik/1.6.0 (Linux; U; Android 4.4.4; GT-S5570 Build/KTU84Q)",
		"Dalvik/1.4.0 (Linux; U; Android 2.3.6; Lenovo A269i Build/GRK39F)",
		"Dalvik/1.6.0 (Linux; U; Android 4.1.2; ST26i Build/11.2.A.0.31)AWEIY511-U30)",
		"Dalvik/1.6.0 (Linux; U; Android 4.2.2; 3G NOTE i Build/JDQ39)",
		"Dalvik/1.6.0 (Linux; U; Android 4.4.2; ASUS_T00Q Build/KVT49L)/CLDC-1.1",
		"Dalvik/1.6.0 (Linux; U; Android 4.4.2; ASUS_T00Q Build/KVT49L)UNTRUSTED/1.0C-1.1; Opera Mini/att/4.2",
		"Dalvik/1.6.0 (Linux; U; Android 4.4.2; ASUS_T00Q Build/KVT49L)0310; Profile/MIDP-2.1 Configuration/C",
		"Dalvik/1.6.0 (Linux; U; Android 4.4.2; ASUS_T00Q Build/KVT49L)",
		"Dalvik/1.6.0 (Linux; U; Android 4.1.2; LG-E410 Build/JZO54K)",
		"Dalvik/1.6.0 (Linux; U; Android 4.0.4; W2430 Build/IMM76D)CLDC-1.1; Opera Mini/att/4.2.22250; U; en-",
		"Dalvik/1.6.0 (Linux; U; Android 4.0.4; W2430 Build/IMM76D)014; Profile/MIDP-2.1 Configuration/CLDC-1",
		"Dalvik/1.6.0 (Linux; U; Android 4.0.4; W2430 Build/IMM76D)",
		"Dalvik/1.1.0 (Linux; U; Android 2.1-update1; E15i Build/2.1.1.A.0.6)",
		"Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-S6310 Build/JZO54K)kman)",
		"Dalvik/1.6.0 (Linux; U; Android 4.4.4; Ascend G510 Build/KTU84Q)",
		"Dalvik/2. 1. 0 (Linux; U; Android 5.0.2; D5503 Build/14.5.A.0.270)",
		"Dalvik/1.6.0 (Linux; U; Android 4.2.2; AFTB Build/JDQ39)",
		"Dalvik/1.6.0 (Linux; U; Android 4.1.2; GT-N8013 Build/JZO54K)",
		"Dalvik/2.1.0 (Linux; U; Android 6.0.1; Nexus Player Build/MMB29T)",
		"Dalvik/2.1.0 (Linux; U; Android 6.0; LG-H815 Build/MRA58k)"
	];
	return user_agents[randint(0,len(user_agents)-1)]
	
def getProxyIP():
    lstIps = ['85.204.175.161:8800', '85.204.175.105:8800', '85.204.174.195:8800', '85.204.175.63:8800', '85.204.172.5:8800', '85.204.172.132:8800', '85.204.174.254:8800', '85.204.173.123:8800', '85.204.175.118:8800', '85.204.172.234:8800', '85.204.172.92:8800', '85.204.173.37:8800', '85.204.173.144:8800', '85.204.175.41:8800', '85.204.175.167:8800', '85.204.174.75:8800', '85.204.175.191:8800', '85.204.175.97:8800', '85.204.175.139:8800', '85.204.173.66:8800', '85.204.172.82:8800', '85.204.174.89:8800', '85.204.175.8:8800', '85.204.174.94:8800', '85.204.173.198:8800']
    proxyIP = lstIps[random.randint(0, (len(lstIps) - 1))]
    proxies={'http': 'http://' + str(proxyIP) }
    return proxies