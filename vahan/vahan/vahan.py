from Crypto.Cipher import AES
from json import dumps,loads
from base64 import b64encode,b64decode
from hashlib import md5
from vahan_utils import getProxyIP,getRandomUserAgent
import requests
import time
import sys

class AESCipher:
    BS = 16
    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        pad = lambda s: s + (self.BS - len(s) % self.BS) * chr(self.BS - len(s) % self.BS)
        raw = pad(raw)
        cipher = AES.new(self.key, AES.MODE_ECB)
        return b64encode(cipher.encrypt(raw))
    
    def decrypt( self, enc ):
        unpad = lambda s : s[0:-ord(s[-1])]
        enc = b64decode(enc)
        cipher = AES.new(self.key, AES.MODE_ECB)
        return unpad(cipher.decrypt( enc))

def auth_token_gen(rc_no,time_stamp):
    salt="xvf49&FaQ8sKs$g%"
    key3="8!XGpFgc;g$$pNb>"
    aes_key3=AESCipher(key3)
    str3=md5((''.join([aes_key3.encrypt(salt),md5(time_stamp).hexdigest()]))).hexdigest()    
    str4=aes_key3.encrypt(time_stamp)  
    str5=aes_key3.encrypt(md5(rc_no).hexdigest())
    auth_token=b64encode(''.join([str3,':',str4,':',str5]))    
    return auth_token

def token_gen(time_stamp):
    key3="8!XGpFgc;g$$pNb>"
    aes_key3=AESCipher(key3)
    token=md5(aes_key3.encrypt(time_stamp)).hexdigest()
    return token

def get_data(reg_no):
    s=requests.session()
    time_stamp=time.strftime('%Y%m%d%H%M%S')   
    auth_token=auth_token_gen(reg_no,time_stamp)
    token=token_gen(time_stamp)

    headers={'Auth-Token':auth_token,
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Content-Length':'66',
    'Content-Language':'en-US',
    'User-Agent':getRandomUserAgent(),
    'Host':'echallanweb.gov.in',
    'Connection':'Keep-Alive',
    'Accept-Encoding':'gzip'}

    post_data=dumps({"reg_no":reg_no,"token":token})

    url="http://echallanweb.gov.in/api/get-my-details"

    r=s.post(url,data=post_data,headers=headers)

    key2='M(#KF$4eB[XPyTVj'
    aes_key2 = AESCipher(key2)
    result=aes_key2.decrypt(r.json().get('result'))
    return result